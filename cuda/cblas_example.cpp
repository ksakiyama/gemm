#include <iostream>
#include <vector>
#include <algorithm>
#include <random>
#include <cblas.h>

const int A_WIDTH  = 1280;
const int A_HEIGHT = 1280;
const int B_WIDTH  = 2560;
const int B_HEIGHT = 1280; // = A_WIDTH
const int C_WIDTH  = B_WIDTH;
const int C_HEIGHT = A_HEIGHT;

int main(int argc, char const *argv[]) {
  // scalar data
  const float alpha = 0.1f;
  const float beta  = 0.1f;

  // matrix size, memory size
  unsigned int size_A = A_WIDTH * A_HEIGHT;
  unsigned int size_B = B_WIDTH * B_HEIGHT;
  unsigned int size_C = C_WIDTH * C_HEIGHT;
  unsigned int mem_size_A = sizeof(float) * size_A;
  unsigned int mem_size_B = sizeof(float) * size_B;
  unsigned int mem_size_C = sizeof(float) * size_C;

  // matrix data
  std::vector<float> A(size_A);
  std::vector<float> B(size_B);
  std::vector<float> C(size_C);

  // generate random number
  // std::random_device rnd;
  std::mt19937 mt(11111);
  std::uniform_real_distribution<float> myrand(0.0, 1.0);

  std::generate(A.begin(), A.end(), [&](){ return myrand(mt); });
  std::generate(B.begin(), B.end(), [&](){ return myrand(mt); });
  std::generate(C.begin(), C.end(), [&](){ return myrand(mt); });

  cblas_sgemm(CblasRowMajor, CblasNoTrans, CblasNoTrans,
    A_HEIGHT, B_WIDTH, A_WIDTH, alpha, &A[0], A_WIDTH, &B[0], B_WIDTH, beta, &C[0], C_WIDTH);

  for (int i = 0; i < 10; i++)
    std::cout << A[i] << std::endl;
  std::cout << "=====" << std::endl;
  for (int i = 0; i < 10; i++)
    std::cout << C[i] << std::endl;

  return 0;
}
