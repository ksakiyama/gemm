#include <iostream>
#include <vector>
#include <algorithm>
#include <random>

#define CUDA_DEVICE (0)

/*
[BEST]
- 980Ti
  - float: 64
  - double: 32
*/
const int NUM_LOOP = 10;

const int BLOCK_SIZE = 64;

const int A_WIDTH  = 1280 * 4;
const int A_HEIGHT = 2560 * 4;
const int B_WIDTH  = 2560 * 4;
const int B_HEIGHT = 1280 * 4; // = A_WIDTH
const int C_WIDTH  = B_WIDTH;
const int C_HEIGHT = A_HEIGHT;

template <typename T> __global__ void
gemm_simple(T *a, T *b, T *c, T alpha, T beta, int aw, int bw)
{
  int x = threadIdx.x + blockIdx.x * blockDim.x;
  int y = threadIdx.y + blockIdx.y * blockDim.y;

  // alpha * AB
  T sum = 0.0;
  for (int i = 0; i < aw; i++) {
    sum += alpha * a[y * aw + i] * b[i * bw + x];
  }

  // C := sum(alpha * AB) + beta * C
  c[y * bw + x] = sum + beta * c[y * bw + x];
}

template <typename T> __global__ void
gemm_opt(T *a, T *b, T *c, T alpha, T beta, int aw, int bw)
{
  __shared__ T tmp_a[BLOCK_SIZE][BLOCK_SIZE];
  __shared__ T tmp_b[BLOCK_SIZE][BLOCK_SIZE];

  int x = threadIdx.x + blockIdx.x * blockDim.x;
  int y = threadIdx.y + blockIdx.y * blockDim.y;
  int lx = threadIdx.x;
  int ly = threadIdx.y;

  // alpha * AB
  T sum = 0.0;
  for (int i = 0; i < aw / BLOCK_SIZE; i++) {
    tmp_a[ly][lx] = a[ aw * y + i * BLOCK_SIZE + lx ];
    tmp_b[ly][lx] = b[(i * BLOCK_SIZE + ly) * bw + x];

    __syncthreads();

    for(int j = 0; j < BLOCK_SIZE; j++) {
      sum += alpha * tmp_a[ly][j] * tmp_b[j][lx];
    }

    __syncthreads();
  }

  // C := sum(alpha * AB) + beta * C
  c[y * bw + x] = sum + beta * c[y * bw + x];
}

template <typename T>
void exec_gemm() {
  // scalar data
  const T alpha = 0.1;
  const T beta  = 0.1;

  // matrix size, memory size
  unsigned int size_A = A_WIDTH * A_HEIGHT;
  unsigned int size_B = B_WIDTH * B_HEIGHT;
  unsigned int size_C = C_WIDTH * C_HEIGHT;
  unsigned int mem_size_A = sizeof(T) * size_A;
  unsigned int mem_size_B = sizeof(T) * size_B;
  unsigned int mem_size_C = sizeof(T) * size_C;

  // matrix data
  std::vector<T> A(size_A);
  std::vector<T> B(size_B);
  std::vector<T> C(size_C);

  // generate random number
  // std::random_device rnd;
  std::mt19937 mt(11111);
  std::uniform_real_distribution<T> myrand(0.0, 1.0);

  std::generate(A.begin(), A.end(), [&](){ return myrand(mt); });
  std::generate(B.begin(), B.end(), [&](){ return myrand(mt); });
  std::generate(C.begin(), C.end(), [&](){ return myrand(mt); });

  // timer
  float kernel_time = 0.0f, memcpy_hd_time = 0.0f, memcpy_dh_time = 0.0f;
  cudaEvent_t kernel_start, kernel_stop;
  cudaEvent_t memcpy_hd_start, memcpy_hd_stop, memcpy_dh_start, memcpy_dh_stop;
  cudaEventCreate(&kernel_start);
  cudaEventCreate(&kernel_stop);
  cudaEventCreate(&memcpy_hd_start);
  cudaEventCreate(&memcpy_hd_stop);
  cudaEventCreate(&memcpy_dh_start);
  cudaEventCreate(&memcpy_dh_stop);


  // allocate device memory
  cudaEventRecord(memcpy_hd_start, 0);

  T *d_A, *d_B, *d_C;
  cudaMalloc((void **) &d_A, mem_size_A);
  cudaMemcpy(d_A, &A[0], mem_size_A, cudaMemcpyHostToDevice);
  cudaMalloc((void **) &d_B, mem_size_B);
  cudaMemcpy(d_B, &B[0], mem_size_B, cudaMemcpyHostToDevice);
  cudaMalloc((void **) &d_C, mem_size_C);
  cudaMemcpy(d_C, &C[0], mem_size_C, cudaMemcpyHostToDevice);

  cudaEventRecord(memcpy_hd_stop, 0);
  cudaEventSynchronize(memcpy_hd_stop);

  dim3 threads(BLOCK_SIZE, BLOCK_SIZE);
  dim3 grid(B_WIDTH / BLOCK_SIZE, A_HEIGHT / BLOCK_SIZE);


  // execute kernel
  cudaEventRecord(kernel_start, 0);
  gemm_opt<T> <<< grid, threads >>> (d_A, d_B, d_C, alpha, beta, A_WIDTH, B_WIDTH);
  cudaDeviceSynchronize();
  cudaEventRecord(kernel_stop, 0);
  cudaEventSynchronize(kernel_stop);

  // get results
  cudaEventRecord(memcpy_dh_start, 0);
  cudaMemcpy(&C[0], d_C, mem_size_C, cudaMemcpyDeviceToHost);
  cudaEventRecord(memcpy_dh_stop, 0);
  cudaEventSynchronize(memcpy_dh_stop);

  //debug
  //for (int i = 0; i < 10; i++)
  //  std::cout << A[i] << std::endl;
  //std::cout << "=====" << std::endl;
  //for (int i = 0; i < 10; i++)
  //  std::cout << C[i] << std::endl;

  // free device memory
  cudaFree(d_A);
  cudaFree(d_B);
  cudaFree(d_C);

  // show details
  cudaEventElapsedTime(&memcpy_hd_time, memcpy_hd_start, memcpy_hd_stop);
  cudaEventElapsedTime(&kernel_time, kernel_start, kernel_stop);
  cudaEventElapsedTime(&memcpy_dh_time, memcpy_dh_start, memcpy_dh_stop);

  printf("%10.5f\t%10.5f\t%10.5f\n", memcpy_hd_time, kernel_time, memcpy_dh_time);
  // printf("Memcpy(H-->D) Time : %10.5f ms\n", memcpy_hd_time);
  // printf("Kernel Exec Time : %10.5f ms\n", kernel_time);
  // printf("Memcpy(D-->H) Time : %10.5f ms\n", memcpy_dh_time);

}

int main(int argc, char const *argv[]) {
  // set cuda device
  int ret = cudaSetDevice(CUDA_DEVICE);
  if (ret != cudaSuccess) {
    std::cerr << "Error:Invalid device" << std::endl;
    return 1;
  }

  printf("#\tmem(h->d)\texeckernel\tmem(d->h)\n");
  for (int i = 0; i < NUM_LOOP; i++) {
    printf("%d\t", i);
    exec_gemm<float>();
  }
  return 0;
}
