#define __CL_ENABLE_EXCEPTIONS

#include <CL/cl.hpp>
#include <iostream>
#include <vector>
#include <cstring>
#include <fstream>
#include <sstream>
#include <typeinfo>
#include <cstdio>
#include <cstdlib>
#include <ctime>

bool cpu_on = false;
bool clcpu_on = false;
bool local_on = false;
bool double_on = false;
cl_uint platform_index = 0;

/* XOR shift RNG */
unsigned int random(unsigned int low, unsigned int high) {
	static unsigned int xor_x = 123456789;
	static unsigned int xor_y = 362436069;
	static unsigned int xor_z = 521288629;
	static unsigned int xor_w = 88675123;
	unsigned int xor_t;
	xor_t = xor_x ^ (xor_x << 11);
	xor_x = xor_y; xor_y = xor_z; xor_z = xor_w;
	xor_w = xor_w ^ (xor_w >> 19) ^ (xor_t ^ (xor_t >> 8));
	return low + (xor_w % (high + 1 - low));
}

template <class T>
T random_t(T low, T high) {
	return low + ( high - low ) * ( random( 0, 1000000 ) / 1000000.0f );
}

double getMiriSec(cl::Event ev) {
	cl_ulong start, end;
	ev.getProfilingInfo(CL_PROFILING_COMMAND_START, &start);
	ev.getProfilingInfo(CL_PROFILING_COMMAND_END  , &end  );
	return (end - start)/1000000.0;
}

template <class T>
void gemm_native(T* dst, T *a, T *b, T *c, T a_param, T b_param, cl_uint width) {
	const cl_uint blockSize = 8;
	clock_t start, end;

	std::cout << "CPU Single Thread" << std::endl;

	start = clock();

	/* ブロッキング */
	for(unsigned int i = 0; i < width; i += blockSize) {
	for(unsigned int j = 0; j < width; j += blockSize) {
	for(unsigned int k = 0; k < width; k += blockSize) {

		for(unsigned int _i = i; _i < i + blockSize; _i++) {
		for(unsigned int _j = j; _j < j + blockSize; _j++) {
		for(unsigned int _k = k; _k < k + blockSize; _k++) {

			dst[_i * width + _j] += a_param * a[_i * width + _k] * b[_k * width + _j];

		}
		}
		}

	}
	}
	}

	for(unsigned int i = 0; i < width * width; i++) {
		dst[i] += c[i] * b_param;
	}

	end = clock();

	for (int i = 0; i < 10; i++) {
		printf("%10.5f\n", dst[i]);
	}

	printf("Time(Clock)    : %10.5f [ms]\n", ((double)end-(double)start) / CLOCKS_PER_SEC * 1000);
}

template <class T>
int gemm(cl_uint width) {
	clock_t start, end;
	start = std::clock();

	cl_uint size = width * width;
	std::string filename, kernelname;
	double memcpyTime = 0, kernelTime = 0;
	cl::NDRange global(width, width), local(16, 16);

	if (double_on) {
		filename = "kernel_d.cl";
	}
	else {
		filename = "kernel.cl";
	}

	if (local_on) {
		kernelname = "gemm_local";
	}
	else {
		kernelname = "gemm";
	}

	/* Host data */
	std::vector<T> a(size, 0);
	std::vector<T> b(size, 0);
	std::vector<T> c(size, 0);
	std::vector<T> results(size, 0);
	T a_param = random_t<T>(0, 10);
	T b_param = random_t<T>(0, 10);

	for (cl_uint i = 0; i < size; i++) {
		a[i] = random_t<T>(0, 10);
		b[i] = random_t<T>(0, 10);
		c[i] = random_t<T>(0, 10);
	}

	if (cpu_on) {
		gemm_native<T>(&results[0], &a[0], &b[0], &c[0], a_param, b_param, width);
		return EXIT_FAILURE;
	}

	try {
		/* Get platform ID */
		std::vector<cl::Platform> platforms;
		cl::Platform::get(&platforms);
		if (platforms.empty()) {
			std::cerr << "No OpenCL platform!" << std::endl;
			return EXIT_FAILURE;
		}

		/* Show platform name */
		for (size_t i = 0; i < platforms.size(); i++) {
			std::string str;
			platforms[i].getInfo(CL_PLATFORM_NAME, &str);
			std::cout << "Platform[" << i << "]: " << str << std::endl;
		}

		if (platform_index >= (cl_uint)platforms.size()) {
			platform_index = 0;
		}

		std::cout << "Select platform " << platform_index << std::endl;

		/* Get device ID */
		std::vector<cl::Device> devices;
		platforms[platform_index].getDevices(clcpu_on ? CL_DEVICE_TYPE_CPU : CL_DEVICE_TYPE_GPU, &devices);
		if (devices.empty()) {
			std::cerr << "No OpenCL device!" << std::endl;
			return EXIT_FAILURE;
		}

		/* Show device name */
		std::string dev_name;
		devices[0].getInfo(CL_DEVICE_NAME, &dev_name);
		std::cout << "Device: " << dev_name << std::endl;

		/* Create context, command-queue */
		cl::Context context(devices);
		cl::CommandQueue queue(context, devices[0], CL_QUEUE_PROFILING_ENABLE);

		/* Read cl file */
		std::ifstream kernelFile(filename.c_str(), std::ios::in);
		if (!kernelFile.is_open()) {
			std::cerr << "Failed to open file for reading: " << filename << std::endl;
			return EXIT_FAILURE;
		}

		std::ostringstream oss;
		oss << kernelFile.rdbuf();

		std::string srcStdStr = oss.str();
		const char *kernelSource = srcStdStr.c_str();

		/* Build program */
		cl::Program::Sources source(1, std::make_pair(kernelSource, strlen(kernelSource)));
		cl::Program program(context, source);
		program.build(devices);

		/* Create kernel */
		cl::Kernel kernel(program, kernelname.c_str());

		cl::Event event_;
		std::vector<cl::Event> events(3, cl::Event());

		/* Create buffers */
		cl::Buffer aBuffer = cl::Buffer(context, CL_MEM_READ_WRITE, sizeof(T) * size);
		cl::Buffer bBuffer = cl::Buffer(context, CL_MEM_READ_WRITE, sizeof(T) * size);
		cl::Buffer cBuffer = cl::Buffer(context, CL_MEM_READ_WRITE, sizeof(T) * size);

		/* Copy values from host to device  */
		queue.enqueueWriteBuffer(aBuffer, CL_TRUE, 0, sizeof(T) * size, &a[0], NULL, &events[0]);
		queue.enqueueWriteBuffer(bBuffer, CL_TRUE, 0, sizeof(T) * size, &b[0], NULL, &events[1]);
		queue.enqueueWriteBuffer(cBuffer, CL_TRUE, 0, sizeof(T) * size, &c[0], NULL, &events[2]);

		for (size_t i = 0; i < events.size(); i++) {
			events[i].wait();
			memcpyTime += getMiriSec(events[i]);
		}

		/* Set arguments */
		kernel.setArg(0, aBuffer);
		kernel.setArg(1, bBuffer);
		kernel.setArg(2, cBuffer);
		kernel.setArg(3, a_param);
		kernel.setArg(4, b_param);
		kernel.setArg(5, width);

		/* Launch kernel */
		queue.enqueueNDRangeKernel(kernel, cl::NullRange, global, local, NULL, &event_);
		event_.wait();
		kernelTime += getMiriSec(event_);

		/* Copy values from device to host */
		queue.enqueueReadBuffer(cBuffer, CL_FALSE, 0, sizeof(T) * size, &results[0], NULL, &event_);
		event_.wait();
		memcpyTime += getMiriSec(event_);


		queue.finish();
		end = std::clock();

		/*
		for (int i = 0; i < 10; i++) {
			printf("%10.5f\n", results[i]);
		}
		*/

		/* Show time */
		printf("Time(Profiling): %10.5f [ms] (memcpy : %10.5f, kernel : %10.5f)\n", memcpyTime+kernelTime, memcpyTime, kernelTime);
		printf("Time(Clock)    : %10.5f [ms]\n", ((double)end-(double)start) / CLOCKS_PER_SEC * 1000);

	} catch (cl::Error err) {
		std::cerr << "Error: " << err.what() << "(" << err.err() << ")" << std::endl;
		return EXIT_FAILURE;
	}

	return EXIT_SUCCESS;
}

int main(int argc, char** argv)
{
	std::cerr << "GEMM OpenCL" << std::endl;
	if (argc < 2) {
		std::cerr << "Usage " << argv[0] << " -cpu -local -double N" << std::endl;
		std::cerr << "\tN: width of matrix(multiply of 16)" << std::endl;
		std::cerr << "\t-cpu: use CPU single thread" << std::endl;
		std::cerr << "\t-clcpu: use CL_DEVICE_TYPE_CPU" << std::endl;
		std::cerr << "\t-local: using local memory" << std::endl;
		std::cerr << "\t-platform #: select platform #\n";
		std::cerr << "\t-double: double-precision" << std::endl;
		return EXIT_FAILURE;
	}

	int start = 1;
	while (start < argc - 1) {
		if (!std::strcmp(argv[start], "-cpu")) {
			cpu_on = true;
		}
		else if (!std::strcmp(argv[start], "-clcpu")) {
			clcpu_on = true;
		}
		else if (!std::strcmp(argv[start], "-local")) {
			local_on = true;
		}
		else if (!std::strcmp(argv[start], "-double")) {
			double_on = true;
		}
		else if(!std::strcmp(argv[start], "-platform") && start < argc - 2) {
			platform_index = std::atoi(argv[start + 1]);
			start++;
		}
		else {
			std::cerr << "Unknown option " << argv[start] << std::endl;
		}
		start++;
	}

	cl_uint width = std::atoi(argv[start]);
	if (width % 16 != 0) {
		std::cerr << "Inalivd width" << std::endl;
		return EXIT_FAILURE;
	}

	std::cout << "Size: " << width << " * " << width << std::endl;

	int ret;
	if (double_on) {
		ret = gemm<double>(width);
	}
	else {
		ret = gemm<float>(width);
	}

	return ret;
}
