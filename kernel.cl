#define BLOCK_SIZE 16

__kernel void gemm(__global float* a,
				   __global float* b,
				   __global float* c,
				   const float alpha,
				   const float beta,
				   const uint width)
{
	const uint x = get_global_id(0);
	const uint y = get_global_id(1);

	float sum = 0.0f;

	for(uint i = 0; i < width; i++)
		sum += alpha * a[ y * width + i ] * b[ i * width + x ];

	c[ y * width + x ] = beta * c[ y * width + x ] + sum;
}

__kernel void gemm_local(__global float* a,
					  __global float* b,
					  __global float* c,
					  const float alpha,
					  const float beta,
					  const uint width)
{
	__local float tmp_a[BLOCK_SIZE][BLOCK_SIZE];
	__local float tmp_b[BLOCK_SIZE][BLOCK_SIZE];

	const uint x = get_global_id(0);
	const uint y = get_global_id(1);
	const uint lx = get_local_id(0);
	const uint ly = get_local_id(1);

	float sum = 0.0f;

	for(uint i = 0; i < width/BLOCK_SIZE; i++)
	{
		tmp_a[ly][lx] = a[ width * y + i * BLOCK_SIZE + lx ];
		tmp_b[ly][lx] = b[(i * BLOCK_SIZE + ly) * width + x];

		barrier(CLK_LOCAL_MEM_FENCE);

		for(uint j = 0; j < BLOCK_SIZE; j++) {
			sum += alpha * tmp_a[ly][j] * tmp_b[j][lx];
		}

		barrier(CLK_LOCAL_MEM_FENCE);
	}

	c[ y * width + x ] = beta * c[ y * width + x ] + sum;
}
